terraform {
  required_providers {
    local = {
      source = "hashicorp/local"
      version = "2.3.0"
    }
  }
}

provider "local" {
  # Configuration options
}
resource "local_file" "testing" {
  filename = "${path.module}/testing/"
  # Agregando un content vacio se crea la carpeta pero hay un error de que es un directorio
  # al hacer un destroy no se borra la carpeta xq no detecta objetos que borrar
  content = ""
  # Segun la documentacion solo es requerido el campo filename
  # al comentar content el error es que requiere otro campo
  # No attribute specified when one (and only one) of [content,sensitive_content,source] is required
}