# terraform_test_directorios


- [] Crear un arbol de directorios con terraform de acuerdo a las siguientes categorias:

UTN_Ingenieria
├── Civil
│   ├── Primer_Año
│   │   ├── Matemática_I
│   │   ├── Física_I
│   │   └── Química
│   ├── Segundo_Año
│   │   ├── Matemática_II
│   │   ├── Física_II
│   │   └── Resistencia_de_Materiales
│   └── Tercer_Año
│       ├── Matemática_III
│       ├── Hidráulica
│       └── Estructuras
├── Industrial
│   ├── Primer_Año
│   │   ├── Matemática_I
│   │   ├── Física_I
│   │   └── Química
│   ├── Segundo_Año
│   │   ├── Matemática_II
│   │   ├── Física_II
│   │   └── Estadística
│   └── Tercer_Año
│       ├── Matemática_III
│       ├── Termodinámica
│       └── Procesos_de_Producción
└── Sistemas
    ├── Primer_Año
    │   ├── Álgebra_y_Geometría_Analítica
    │   ├── Programación_I
    │   └── Introducción_a_la_Ingeniería
    ├── Segundo_Año
    │   ├── Matemática_Discreta
    │   ├── Programación_II
    │   └── Estructuras_de_Datos
    └── Tercer_Año
        ├── Bases_de_Datos
        ├── Sistemas_Operativos
        └── Ingeniería_de_Software

Update 08/05/2024: todavía no logro crear una sola carpeta/directorio y luego borrarla con destroy